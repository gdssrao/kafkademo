package com.javainuse.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javainuse.service.KafkaSender;




@RestController
@RequestMapping(value = "/javainuse-kafka/")
public class ApacheKafkaWebController {

	private HttpServletRequest request;
	
	@Autowired
	KafkaSender kafkaSender;
	
	@RequestMapping(value="/producer",
			method= RequestMethod.POST)
	public String producer(@RequestBody String payload, @RequestHeader(value="topic") String topic) {
		kafkaSender.send(payload,topic);
	
     

		return "Payload :"+payload +" Topic :"+topic;
	}

}