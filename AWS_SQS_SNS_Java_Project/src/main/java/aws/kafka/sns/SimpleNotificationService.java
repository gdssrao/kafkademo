package aws.kafka.sns;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.PublishRequest;

public class SimpleNotificationService {

    public static void main(String[] args) throws Exception {

        ProfileCredentialsProvider credentialsProvider = new ProfileCredentialsProvider();
      
        AmazonSNS service = AmazonSNSClient.builder()
                               .withCredentials(credentialsProvider)
                               .withRegion(Regions.AP_SOUTHEAST_2)
                               .build();
        
        // Create a topic
        CreateTopicRequest createReq = new CreateTopicRequest()
            .withName("AWS_Topic");
        CreateTopicResult createRes = service.createTopic(createReq);
        
        PublishRequest publishReq = new PublishRequest()
                .withTopicArn(createRes.getTopicArn())
                .withMessage("Kafka Message Sent to Topic");
            service.publish(publishReq);

            System.out.println("Sent message to AWS_TOPIC.\n");
    }
}
