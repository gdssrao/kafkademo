package aws.kafka.sqs;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;


public class SimpleQueueService {

    public static void main(String[] args) throws Exception {

        ProfileCredentialsProvider credentialsProvider = new ProfileCredentialsProvider();

        AmazonSQS sqs = AmazonSQSClientBuilder.standard()
                               .withCredentials(credentialsProvider)
                               .withRegion(Regions.AP_SOUTHEAST_2)
                               .build();

           String QueueName = "TEMP_QUEUE";
            String myQueueUrl = sqs.getQueueUrl(QueueName).getQueueUrl();

            // Send a message         
            System.out.println("Sending a message to TEMP_QUEUE.\n");     
      
            sqs.sendMessage(new SendMessageRequest(myQueueUrl, "Kafka Message to AWS"));
     
        
    }
}
